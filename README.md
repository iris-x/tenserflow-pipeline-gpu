
# Tensorflow 2.X GPU Environment
Base image for Tensorflow 2.0, targeting a GPU environment for Kubeflow 0.6.
Contains miscellanous ML packages as well. Update requirements.txt if dependencies are missing.

## Building 
Once pushed to master the new image will be built and pushed to AI-platforms Docker repository.

## Usage

When spinning up a new notebook-server in Kubeflow check Custom Image and type: 10.104.88.155:5000/iris-x/tenserflow-pipeline-gpu